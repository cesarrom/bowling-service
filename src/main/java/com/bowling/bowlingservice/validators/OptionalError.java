package com.bowling.bowlingservice.validators;

public class OptionalError<ClassType, ErrorType extends Throwable> {
	private ErrorType error;
	private ClassType val;

	private OptionalError(ClassType val, ErrorType error) {
		this.error = error;
		this.val = val;	
	}
	
	public Boolean hasError() {
		return this.error != null;
	}
	
	public ClassType getOrThrow() throws ErrorType {
		if (this.hasError()) throw this.error;
		return this.val;
	}
	
	public ClassType getValOrNull() {
		return this.val;
	}
	
	public void printError() {
		if (this.hasError())
			this.error.printStackTrace();
	}
	
	public String getErrorMessage() {
		if (this.hasError())
			return this.error.getMessage();
		return "";
	}
	
	
	public static <T, ErrorType extends Throwable> OptionalError<T, ErrorType> fromError(ErrorType error) {
		return new OptionalError<>(null, error);
	}
	
	public static <ClassType, E extends Throwable> OptionalError<ClassType, E> fromValue(ClassType val) {
		return new OptionalError<>(val, null);
	}
	
	public static <ClassType, ErrorType extends Throwable> OptionalError<ClassType, ErrorType> create(ClassType val, ErrorType error) {
		return new OptionalError<>(val, error);
	}
}
