package com.bowling.bowlingservice.validators;

import java.util.regex.Pattern;

public class UtilValidator {
	private static final Pattern NUM_PATTERN = Pattern.compile("-?\\d+(\\.\\d+)?");
	
	public static boolean isNumeric(String strNum) {
	    if (strNum == null) {
	        return false; 
	    }
	    return NUM_PATTERN.matcher(strNum).matches();
	}
}
