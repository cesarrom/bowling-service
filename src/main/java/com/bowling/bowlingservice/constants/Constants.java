package com.bowling.bowlingservice.constants;

import com.bowling.bowlingservice.model.Frame;
import com.bowling.bowlingservice.model.impl.dto.NullFrameDto;

public class Constants {
	public static final Frame NULL_FRAME = new NullFrameDto();
	
}
