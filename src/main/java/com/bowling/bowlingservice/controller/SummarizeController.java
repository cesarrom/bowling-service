package com.bowling.bowlingservice.controller;

import java.io.InputStream;
import java.util.Set;

import com.bowling.bowlingservice.model.GameSummary;

public interface SummarizeController {
	public Set<GameSummary> summarizeFile(InputStream stream) throws Throwable;
}
