package com.bowling.bowlingservice.controller.shell;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bowling.bowlingservice.controller.SummarizeController;
import com.bowling.bowlingservice.model.GameSummary;

import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Component
@Command(name = "summarize")
public class SummarizeShellController implements Runnable {
	private final PrintStream console;
	private final SummarizeController controller;

	@Autowired
	public SummarizeShellController(SummarizeController controller, Optional<PrintStream> console) {
		this.controller = controller;
		this.console = console.orElse(System.out);
	}

	@Option(names = { "-p", "--path" }, description = "the path to the file to be reed")
	public String path;

	public void run() {
		try {
			if (path == null) {
				throw new RuntimeException(
						"No path was provided, remember using '-p' param to provide a path string to the file to summarize");
			}
			
			Path pathObject = Paths.get(path);
			String relativePathPreffix = System.getProperty("user.dir");
			Path relativeObj = Paths.get(String.join("/", relativePathPreffix, path));
			
			Path correctPath = null;
			
			if (Files.exists(pathObject)) {
				correctPath = pathObject;
			} else if (Files.exists(relativeObj)) {
				correctPath = relativeObj;
			} else {
				throw new RuntimeException("File does not exists");
			}

			if (Files.isDirectory(correctPath)) {
				throw new RuntimeException("A File was expected but a directoy was provided");
			}
			
			try (InputStream is = new FileInputStream(correctPath.toString())) {

				Set<GameSummary> summarized = controller.summarizeFile(is);

				if (summarized == null || summarized.size() == 0) {
					throw new RuntimeException("summary returned nothing");
				}

				int index = 0;
				for (GameSummary summary : summarized) {
					if (index == 0) {
						List<String> frames = new ArrayList<>();
						frames.add("Frames");
						for (Integer k = 1; k <= summary.getTotalFrames(); k++) {
							frames.add(k.toString());
						}
						this.console.println(String.join("\t\t", frames));
					}

					this.console.println(summary.getPlayerName().trim());
					this.console.println(("Pinfalls\t" + String.join("\t", summary.getShots())).trim());
					this.console.println(("Score\t\t" + String.join("\t",
							summary.getSummarizedScore().stream().map(s -> s + "\t").collect(Collectors.toList()))).trim());
					index++;
				}
				this.console.println();

			} catch (Throwable e) {
				String message = e.getMessage();
				this.console.println(message);
			}
		} catch (Throwable e) {
			String message = e.getMessage();
			this.console.println(message);
		}
	}
}
