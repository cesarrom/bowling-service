package com.bowling.bowlingservice.controller.impl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.bowling.bowlingservice.business.FileParser;
import com.bowling.bowlingservice.business.GameSummaryFactory;
import com.bowling.bowlingservice.controller.SummarizeController;
import com.bowling.bowlingservice.model.GameSummary;
import com.bowling.bowlingservice.model.Player;

@Controller
public class DefaultSummarizeController implements SummarizeController {
	private FileParser parser;
	private GameSummaryFactory factory;
	
	@Autowired
	public DefaultSummarizeController(FileParser parser, GameSummaryFactory factory) {
		this.parser = parser;
		this.factory = factory;
	}
	
	public Set<GameSummary> summarizeFile(InputStream stream) throws Throwable {		
		try (Reader reader = new InputStreamReader(stream)) {
			BufferedReader bufferedReader = new BufferedReader(reader);
			Set<Player> players = parser.parse(bufferedReader);
			return players
					.stream()
					.map(player -> this.factory.create(player.getName(), player.getFrames()))
					.collect(Collectors.toSet());
		} catch (Throwable e) {
			throw e;
		}
	}
}
