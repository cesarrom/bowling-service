package com.bowling.bowlingservice.model;

import java.util.List;

public interface Frame {
	
	Integer getFrameScore();
	
	Integer getScore();
		
	Integer getAttemptScore(Integer attemptNumber);

	Integer getAttemptSize();

	List<Integer> getAttempts();
	
	Integer getSummarizedScore();

	Integer getScoreDepth(Long number);

	List<Integer> rebuildAttempts();
	
	Frame getNext();
	
	Frame getPrev();
	
	void setNext(Frame next);
	
	void setPrev(Frame prev);
}
