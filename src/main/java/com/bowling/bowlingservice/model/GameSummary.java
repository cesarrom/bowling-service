package com.bowling.bowlingservice.model;

import java.util.List;

public interface GameSummary {
	Integer getTotalFrames();
	Integer getExtraAttempts();
	String getPlayerName();
	List<String> getShots();
	List<String> getScorePerFrame();
	List<String> getSummarizedScore();
}
