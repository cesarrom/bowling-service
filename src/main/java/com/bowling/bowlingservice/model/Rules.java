package com.bowling.bowlingservice.model;

public interface Rules {
	Integer getMaxAttempts();
	Integer getFrameCount();
	Integer getMaxExtraAttempts();
	Integer getPinCount();
	Integer getMaxScore();
	Integer getMaxFrameScore();
}
