package com.bowling.bowlingservice.model;

import java.util.List;

public interface Player {
	String getName();
	List<Frame> getFrames();
}
