package com.bowling.bowlingservice.model.impl.dto;

import java.util.ArrayList;
import java.util.List;

import com.bowling.bowlingservice.model.Frame;

import lombok.experimental.SuperBuilder;

@SuperBuilder
public class SpareFrameDto extends FrameDto {
	
	public SpareFrameDto(List<Integer> attempts, Frame parent, Frame prev) {
		super(attempts, parent, prev);
	}
	
	public SpareFrameDto(List<Integer> attempts) {
		super(attempts, null, null);
	}

	@Override
	public Integer getScore() {
		return this.getFrameScore() + getNext().getScoreDepth(1L);
	}

	@Override
	public String toString() {
		List<String> list = new ArrayList<>();
		for (int i = 0; i < this.getAttemptSize(); i++) {
			list.add(
				i == this.getAttemptSize() - 1 ? "/" : this.getAttemptScore(i).toString()
					
			);
		}
		return String.join("\t", list);
	}
}
