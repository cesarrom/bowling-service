package com.bowling.bowlingservice.model.impl.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.bowling.bowlingservice.constants.Constants;
import com.bowling.bowlingservice.model.Frame;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class FrameDto implements Frame {
	private List<Integer> attempts;
	private Frame next;
	private Frame prev;

	public FrameDto(List<Integer> attempts, Frame parent, Frame prev) {
		this.attempts = attempts;
		this.next = parent != null ? parent : Constants.NULL_FRAME;
		this.prev = prev != null ? prev : Constants.NULL_FRAME;
	}
	
	public FrameDto(List<Integer> attempts) {
		this.attempts = attempts;
		this.next = Constants.NULL_FRAME;
		this.prev = Constants.NULL_FRAME;
	}

	@Override
	public Integer getAttemptScore(Integer attemptNumber) {
		if (attemptNumber >= this.attempts.size()) {
			return 0;
		}
		return this.attempts.get(attemptNumber);
	}

	@Override
	public Integer getAttemptSize() {
		return this.attempts.size();
	}

	@Override
	public Integer getFrameScore() {
		return this.attempts.stream().reduce(Integer::sum).orElse(0);
	}

	@Override
	public List<Integer> rebuildAttempts() {
		List<Integer> attempts = new ArrayList<>(this.getAttempts());
		attempts.addAll(this.next.rebuildAttempts());
		return attempts;
	}

	@Override
	public Integer getScoreDepth(Long number) {
		return this.rebuildAttempts().stream().skip(0L).limit(number).reduce(Integer::sum).orElse(0);
	}

	@Override
	public Integer getScore() {
		return this.getFrameScore();
	}
	
	@Override
	public String toString() {
		return String.join("\t", this.attempts.stream().map(a -> a.toString()).collect(Collectors.toList()));
	}

	@Override
	public Integer getSummarizedScore() {
		return this.prev.getSummarizedScore() + this.getScore();
	}

}
