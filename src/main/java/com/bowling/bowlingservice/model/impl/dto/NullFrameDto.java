package com.bowling.bowlingservice.model.impl.dto;

import java.util.Arrays;
import java.util.List;

import com.bowling.bowlingservice.model.Frame;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class NullFrameDto implements Frame {
	public NullFrameDto() {
	}

	@Override
	public Integer getAttemptScore(Integer attemptNumber) {
		return 0;
	}

	@Override
	public Integer getAttemptSize() {
		return 0;
	}

	@Override
	public Integer getFrameScore() {
		return 0;
	}

	@Override
	public List<Integer> rebuildAttempts() {
		return Arrays.asList();
	}

	@Override
	public Integer getScoreDepth(Long number) {
		return 0;
	}

	@Override
	public Integer getScore() {
		return 0;
	}

	@Override
	public List<Integer> getAttempts() {
		return Arrays.asList();
	}

	@Override
	public Integer getSummarizedScore() {
		return 0;
	}

	@Override
	public Frame getNext() {
		return this;
	}

	@Override
	public Frame getPrev() {
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public void setNext(Frame next) {
		return;
	}

	@Override
	public void setPrev(Frame prev) {
		return;
	}
	

}
