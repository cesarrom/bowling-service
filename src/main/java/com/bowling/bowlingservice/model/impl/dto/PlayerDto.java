package com.bowling.bowlingservice.model.impl.dto;

import java.util.List;

import com.bowling.bowlingservice.model.Frame;
import com.bowling.bowlingservice.model.Player;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class PlayerDto implements Player {
	private String name;
	private List<Frame> frames;

	public PlayerDto(String name, List<Frame> frames) {
		this.name = name;
		this.frames = frames;
	}

	@Override
	public String toString() {
		return this.name;
	}
}
