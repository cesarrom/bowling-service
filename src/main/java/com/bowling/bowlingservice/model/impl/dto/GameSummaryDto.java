package com.bowling.bowlingservice.model.impl.dto;

import java.util.List;

import com.bowling.bowlingservice.model.GameSummary;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GameSummaryDto implements GameSummary {
	private String playerName;
	private List<String> shots;
	private List<String> scorePerFrame;
	private List<String> summarizedScore;
	private Integer totalFrames;
	private Integer extraAttempts;
}
