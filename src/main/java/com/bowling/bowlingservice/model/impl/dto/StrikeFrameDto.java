package com.bowling.bowlingservice.model.impl.dto;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.bowling.bowlingservice.model.Frame;

import lombok.experimental.SuperBuilder;

@SuperBuilder
public class StrikeFrameDto extends FrameDto {

	
	public StrikeFrameDto(List<Integer> attempts, Frame parent, Frame prev) {
		super(attempts, parent, prev);
	}
	
	public StrikeFrameDto(List<Integer> attempts) {
		super(attempts, null, null);
	}
	
	@Override
	public List<Integer> getAttempts() {
		return Arrays.asList(
				this.getAttemptScore(0)
		);
	}

	@Override
	public Integer getScore() {
		return this.getFrameScore() + getNext().getScoreDepth(2L);
	}
	
	@Override
	public String toString() {
		List<String> list = new ArrayList<>();
		for (int i = 0; i < this.getAttemptSize(); i++) {
			list.add(
				i == this.getAttemptSize() - 1 ? "X" : ""
					
			);
		}
		return String.join("\t", list);
	}
	

}
