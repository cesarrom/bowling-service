package com.bowling.bowlingservice.business;

import com.bowling.bowlingservice.model.Rules;

public interface RulesProvider {
	Rules getRules();
}
