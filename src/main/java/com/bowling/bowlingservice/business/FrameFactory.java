package com.bowling.bowlingservice.business;

import java.util.List;

import com.bowling.bowlingservice.model.Frame;

public interface FrameFactory {
	Frame create(List<Integer> attempts, Frame nextFrame, Frame prevFrame, Integer frameNumber);
}
