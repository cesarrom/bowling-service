package com.bowling.bowlingservice.business;

import java.io.BufferedReader;
import java.util.Set;

import com.bowling.bowlingservice.model.Player;

public interface FileParser {
	Set<Player> parse(BufferedReader reader);
}
