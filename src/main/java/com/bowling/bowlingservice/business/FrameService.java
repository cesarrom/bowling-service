package com.bowling.bowlingservice.business;

import java.util.List;

import com.bowling.bowlingservice.model.Frame;

public interface FrameService {
	List<Frame> parseFrames(List<Integer> frames);
}
