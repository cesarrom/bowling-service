package com.bowling.bowlingservice.business;

import java.util.List;

import com.bowling.bowlingservice.model.Frame;
import com.bowling.bowlingservice.model.Player;

public interface PlayerFactory {
	Player create(String name, List<Frame> frames);
}
