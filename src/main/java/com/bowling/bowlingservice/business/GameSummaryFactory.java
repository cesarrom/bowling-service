package com.bowling.bowlingservice.business;

import java.util.List;

import com.bowling.bowlingservice.model.Frame;
import com.bowling.bowlingservice.model.GameSummary;

public interface GameSummaryFactory {
	GameSummary create(String playerName, List<Frame> frames);
}
