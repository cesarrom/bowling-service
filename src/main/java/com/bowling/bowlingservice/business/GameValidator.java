package com.bowling.bowlingservice.business;

import java.util.List;

import com.bowling.bowlingservice.model.Frame;

public interface GameValidator {
	public void validateFrame(List<Frame> frames);

	public void validateFrame(List<Frame> frames, Integer frameNumber);

}
