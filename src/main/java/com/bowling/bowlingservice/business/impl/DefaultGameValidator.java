package com.bowling.bowlingservice.business.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bowling.bowlingservice.business.GameValidator;
import com.bowling.bowlingservice.business.RulesProvider;
import com.bowling.bowlingservice.model.Frame;
import com.bowling.bowlingservice.model.Rules;

@Service
public class DefaultGameValidator implements GameValidator {
	private final RulesProvider rulesProvider;

	@Autowired
	public DefaultGameValidator(final RulesProvider rulesProvider) {
		this.rulesProvider = rulesProvider;
	}

	@Override
	public void validateFrame(List<Frame> frames) {
		Rules rules = rulesProvider.getRules();
		Integer totalAttemtps = frames.stream().map(f -> f.getAttemptSize()).reduce(Integer::sum).orElse(0);
		Integer desiredTotalAttemps = (rules.getMaxAttempts() * rules.getFrameCount()) + rules.getMaxExtraAttempts();

		if (totalAttemtps > desiredTotalAttemps) {
			throw new RuntimeException("One player played more frames than expected");
		}

		if (!rules.getFrameCount().equals(frames.size())) {
			throw new RuntimeException("Incorrect number of frames provided by player, needed " + rules.getFrameCount()
					+ " but received " + frames.size());
		}
		
		for(int frameNumber = 0; frameNumber < frames.size(); frameNumber++) {
			if (rules.getFrameCount().equals(frameNumber + 1))
				this.validateFrame(frames, frameNumber, true);
			else
				this.validateFrame(frames, frameNumber);
		}
	}
	
	private void validateFrame(List<Frame> frames, Integer frameNumber, Boolean isLast) {
		Frame frame = frames.get(frameNumber);

		Rules rules = rulesProvider.getRules();
		Integer totalAttempts = rules.getMaxAttempts() + (isLast ? rules.getMaxExtraAttempts() : 0);

		Integer localScore = frame.getFrameScore();
		Integer maxLocalScore = isLast ? rules.getPinCount() * totalAttempts : rules.getPinCount();
				
		if (localScore > maxLocalScore) {
			throw new RuntimeException("Shot Pinfall score (" + localScore + ") on frame " + (frameNumber + 1)
					+ " is out of range 0-" + rules.getPinCount());
		}
		
		Integer specialScore = frame.getScore();

		if (specialScore > rules.getMaxFrameScore()) {
			throw new RuntimeException("Frame score (" + specialScore + ") on frame " + (frameNumber + 1)
					+ " is out of range 0-" + rules.getMaxFrameScore());
		}

		Integer summarizedScore = frame.getSummarizedScore();

		if (summarizedScore > rules.getMaxScore()) {
			throw new RuntimeException("Accumulated score (" + summarizedScore + ") on frame " + (frameNumber + 1)
					+ " is out of range 0-" + rules.getMaxScore());
		}

	}

	@Override
	public void validateFrame(List<Frame> frames, Integer frameNumber) {
		this.validateFrame(frames, frameNumber, false);
	}

}
