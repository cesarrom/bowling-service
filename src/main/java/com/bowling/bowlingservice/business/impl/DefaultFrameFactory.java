package com.bowling.bowlingservice.business.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bowling.bowlingservice.business.FrameFactory;
import com.bowling.bowlingservice.business.RulesProvider;
import com.bowling.bowlingservice.model.Frame;
import com.bowling.bowlingservice.model.impl.dto.FrameDto;
import com.bowling.bowlingservice.model.impl.dto.SpareFrameDto;
import com.bowling.bowlingservice.model.impl.dto.StrikeFrameDto;

@Service
public class DefaultFrameFactory implements FrameFactory {
	private final RulesProvider rulesProvider;
	
	@Autowired
	public DefaultFrameFactory(final RulesProvider rulesProvider) {
		this.rulesProvider = rulesProvider;
	}

	Boolean isStrike(List<Integer> attempts) {
		return attempts.get(0) == rulesProvider.getRules().getPinCount();
	}

	Boolean isSpare(List<Integer> attempts) {
		return attempts.stream().reduce(Integer::sum).orElse(0) == rulesProvider.getRules().getPinCount();
	}

	Boolean isLast(Integer frameNumber) {
		return frameNumber + 1 >= this.rulesProvider.getRules().getFrameCount();
	}

	@Override
	public Frame create(List<Integer> attempts, Frame nextFrame, Frame prevFrame, Integer frameNumber) {
		if (isLast(frameNumber)) {
			return new FrameDto(attempts, nextFrame, prevFrame);
		}
		if (isStrike(attempts)) {
			return new StrikeFrameDto(attempts, nextFrame, prevFrame);
		}
		if (isSpare(attempts)) {
			return new SpareFrameDto(attempts, nextFrame, prevFrame);
		}
		return new FrameDto(attempts, nextFrame, prevFrame);
	}

}
