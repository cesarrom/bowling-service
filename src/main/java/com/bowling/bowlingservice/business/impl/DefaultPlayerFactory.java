package com.bowling.bowlingservice.business.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bowling.bowlingservice.business.PlayerFactory;
import com.bowling.bowlingservice.model.Frame;
import com.bowling.bowlingservice.model.Player;
import com.bowling.bowlingservice.model.impl.dto.PlayerDto;

@Service
public class DefaultPlayerFactory implements PlayerFactory {

	@Override
	public Player create(String name, List<Frame> frames) {
		return new PlayerDto(name, frames);
	}
	
}
