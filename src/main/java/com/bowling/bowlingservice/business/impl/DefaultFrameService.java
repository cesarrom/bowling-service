package com.bowling.bowlingservice.business.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bowling.bowlingservice.business.FrameFactory;
import com.bowling.bowlingservice.business.FrameService;
import com.bowling.bowlingservice.business.GameValidator;
import com.bowling.bowlingservice.business.RulesProvider;
import com.bowling.bowlingservice.model.Frame;

@Service
public class DefaultFrameService implements FrameService {
	
	private final RulesProvider rulesProvider;
	private final FrameFactory frameFactory;
	private final GameValidator validator;
	
	@Autowired
	public DefaultFrameService(final RulesProvider rulesProvider, final FrameFactory frameFactory, final GameValidator validator) {
		this.rulesProvider = rulesProvider;
		this.frameFactory = frameFactory;
		this.validator = validator;
	}
	
	private List<List<Integer>> getAugmentedAttempts(List<Integer> attempts) {
		List<List<Integer>> result = new ArrayList<>();

		Iterator<Integer> iterator = attempts.iterator();
		List<Integer> current = new ArrayList<>();
		int frameNumber = 1;

		while (iterator.hasNext()) {
			current = new ArrayList<>();
			Integer crr = iterator.next();
			int maxNumberOfAttempts = this.rulesProvider.getRules().getMaxAttempts();
			
			if (this.rulesProvider.getRules().getFrameCount().equals(frameNumber)) {
				current.add(crr);
				maxNumberOfAttempts += this.rulesProvider.getRules().getMaxExtraAttempts();
				while(iterator.hasNext() || current.size() < maxNumberOfAttempts) current.add(iterator.hasNext() ? iterator.next() : 0);
			} else if (crr.equals(this.rulesProvider.getRules().getPinCount())) {
				current.add(crr);
				while(current.size() < maxNumberOfAttempts) current.add(0);
			} else {
				current.add(crr);
				while(current.size() < maxNumberOfAttempts) current.add(iterator.hasNext() ? iterator.next() : 0);
			}
			
			frameNumber++;
			result.add(current);
		}
		
		return result;

	}


	@Override
	public List<Frame> parseFrames(List<Integer> attempts) {		
		List<List<Integer>> augAttempts = this.getAugmentedAttempts(attempts);
		
		List<Frame> frames = new ArrayList<>();
		
		// create with next frame reference
		for (int i = augAttempts.size() - 1; i >= 0; i--) {
			List<Integer> currentAttempts = augAttempts.get(i);
			
			if (i == augAttempts.size() - 1) {
				frames.add(
						this.frameFactory.create(currentAttempts, null, null, i)
				);
			} else {
				Frame parentFrame = frames.get(frames.size() - 1);
				frames.add(
						this.frameFactory.create(currentAttempts, parentFrame, null, i)
				);
			}
		}
		
		Collections.reverse(frames);
		
		// complete array with prev reference
		for (int i = 1; i < augAttempts.size(); i++) {
			Frame crr = frames.get(i);
			Frame prev = frames.get(i - 1);
			crr.setPrev(prev);
		}
		
		validator.validateFrame(frames);
		
		return frames;
	}

}
