package com.bowling.bowlingservice.business.impl;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bowling.bowlingservice.business.FileParser;
import com.bowling.bowlingservice.business.FrameService;
import com.bowling.bowlingservice.business.PlayerFactory;
import com.bowling.bowlingservice.business.RulesProvider;
import com.bowling.bowlingservice.model.Player;
import com.bowling.bowlingservice.model.Rules;
import com.bowling.bowlingservice.validators.UtilValidator;

@Service
public class DefaultFileParser implements FileParser {
	public static final Pattern LINE_FORMAT = Pattern.compile("[a-zA-Z_]+\\s[a-zA-Z0-9_]+", Pattern.CASE_INSENSITIVE);
	private PlayerFactory playerFactory;

	private final RulesProvider ruleProvider;
	
	private final FrameService frameService;
	
	@Autowired
	public DefaultFileParser(final PlayerFactory playerFactory, final RulesProvider ruleProvider, final FrameService frameService) {
		this.playerFactory = playerFactory;
		this.ruleProvider = ruleProvider;
		this.frameService = frameService;
	}

	private Integer parseScore(String score) {
		Rules rules = ruleProvider.getRules();
		if ("F".equals(score)) {
			return 0;
		}

		if ("X".equals(score)) {
			return rules.getPinCount();
		}
		
		if (!UtilValidator.isNumeric(score)) {
			throw new RuntimeException("Value: " + score + " is not valid, please try with X, F or a positive number");
		}
		
		Integer result = Integer.parseInt(score);
		
		if (result < 0) {
			throw new RuntimeException("Value: " + score + " is not valid, please try with X, F or a positive number");
		}

		return result;
	}

	@Override
	public Set<Player> parse(BufferedReader reader) {

		final Map<String, Player> playerByName = new HashMap<>();
		final Map<Player, List<Integer>> players = new HashMap<>();

		reader.lines().forEach(line -> {
			if (line.replaceAll("\\n", "").replaceAll("\\r", "").trim().isEmpty()) { return; }
			
			Matcher matcher = DefaultFileParser.LINE_FORMAT.matcher(line);
		    boolean matchFound = matcher.find();
		    
			if (!matchFound) {
				throw new RuntimeException("The file contains lines with wrong format");
			}
			
			String[] lineArr = line.split(" ");
			String playerName = lineArr[0];
			String score = lineArr[1];

			playerByName.computeIfAbsent(playerName, (k) -> this.playerFactory.create(playerName, new ArrayList<>()));

			Player player = playerByName.get(playerName);

			players.computeIfAbsent(player, (k) -> new ArrayList<>());

			players.computeIfPresent(player, (k, o) -> {
				o.add(this.parseScore(score));
				return o;
			});
		});

		return players.entrySet().stream().map(entry -> {
			Player player = entry.getKey();
			return this.playerFactory.create(
					player.getName(),
					this.frameService.parseFrames(
							entry.getValue()
						)
				);
		}).collect(Collectors.toSet());
	}

}
