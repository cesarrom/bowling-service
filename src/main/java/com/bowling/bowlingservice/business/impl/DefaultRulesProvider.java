package com.bowling.bowlingservice.business.impl;

import org.springframework.stereotype.Service;

import com.bowling.bowlingservice.business.RulesProvider;
import com.bowling.bowlingservice.model.Rules;
import com.bowling.bowlingservice.model.impl.dto.RulesDto;

@Service
public class DefaultRulesProvider implements RulesProvider {
	
	private RulesDto rules;

	public DefaultRulesProvider() {
		this.rules = new RulesDto();
		this.rules.setMaxExtraAttempts(1);
		this.rules.setFrameCount(10);
		this.rules.setMaxAttempts(2);
		this.rules.setPinCount(10);
		this.rules.setMaxScore(300);
		this.rules.setMaxFrameScore(30);
	}

	@Override
	public Rules getRules() {
		return this.rules;
	}

}
