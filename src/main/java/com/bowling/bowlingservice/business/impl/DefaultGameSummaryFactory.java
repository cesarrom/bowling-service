package com.bowling.bowlingservice.business.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bowling.bowlingservice.business.GameSummaryFactory;
import com.bowling.bowlingservice.business.RulesProvider;
import com.bowling.bowlingservice.model.Frame;
import com.bowling.bowlingservice.model.GameSummary;
import com.bowling.bowlingservice.model.Rules;
import com.bowling.bowlingservice.model.impl.dto.GameSummaryDto;

@Component
public class DefaultGameSummaryFactory implements GameSummaryFactory {

	private RulesProvider provider;

	@Autowired
	public DefaultGameSummaryFactory(RulesProvider provider) {
		this.provider = provider;
	}

	@Override
	public GameSummary create(String playerName, List<Frame> frames) {
		Rules rules = this.provider.getRules();

		return GameSummaryDto.builder().playerName(playerName).totalFrames(rules.getFrameCount())
				.extraAttempts(rules.getMaxExtraAttempts())
				.scorePerFrame(frames.stream().map(f -> f.getScore().toString()).collect(Collectors.toList()))
				.shots(frames.stream().map(f -> f.toString()).collect(Collectors.toList()))
				.summarizedScore(
						frames.stream().map(f -> f.getSummarizedScore().toString()).collect(Collectors.toList()))
				.build();
	}

}
