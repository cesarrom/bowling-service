package com.bowling.bowlingservice;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.bowling.bowlingservice.business.impl.DefaultFrameFactory;
import com.bowling.bowlingservice.business.impl.DefaultGameValidator;
import com.bowling.bowlingservice.constants.Constants;
import com.bowling.bowlingservice.model.Frame;
import com.bowling.bowlingservice.model.Rules;
import com.bowling.bowlingservice.model.impl.dto.FrameDto;
import com.bowling.bowlingservice.model.impl.dto.RulesDto;

public class DefaultGameValidatorTest {

	private final Rules TenPinsRules = RulesDto
			.builder()
			.frameCount(10)
			.maxAttempts(2)
			.maxExtraAttempts(1)
			.maxScore(300)
			.maxFrameScore(30)
			.pinCount(10)
			.build();

	private List<Frame> buildFrames(Rules rules) {
		DefaultFrameFactory factory = new DefaultFrameFactory(() -> rules);
		Random random = new Random();
		List<Frame> frames = new ArrayList<>();

		for (int i = 0; i < rules.getFrameCount(); i++) {
			List<Integer> attempts = new ArrayList<>();
			int pinsLeft = rules.getPinCount();
			for (int k = 0; k < rules.getMaxAttempts(); k++) {
				int knockedPins = random.nextInt(pinsLeft);
				pinsLeft -= knockedPins;
				attempts.add(knockedPins);
			}
			if (i == rules.getFrameCount() - 1) {
				for (int w = 0; w < rules.getMaxExtraAttempts(); w++) {
					attempts.add(random.nextInt(rules.getPinCount()));
				}
			}
			frames.add(factory.create(attempts, null, i == 0 ? Constants.NULL_FRAME : frames.get(i - 1), i));
		}

		for (int i = 0; i < frames.size(); i++) {
			Frame frame = frames.get(i);
			frame.setNext(i == frames.size() - 1 ? Constants.NULL_FRAME : frames.get(i + 1));
		}

		return frames;
	}

	@Test
	public void validateFrames() {
		final DefaultGameValidator validator = new DefaultGameValidator(() -> this.TenPinsRules);
		
		assertDoesNotThrow(() -> {
			List<Frame> frames = this.buildFrames(this.TenPinsRules);
			validator.validateFrame(frames);
		});
		
		assertThrows(Throwable.class, () -> {
			List<Frame> frames = this.buildFrames(this.TenPinsRules);
			frames.add(FrameDto.builder().attempts(Arrays.asList()).prev(Constants.NULL_FRAME).next(Constants.NULL_FRAME).build());
			validator.validateFrame(frames);
		});
		
		assertThrows(Throwable.class, () -> {
			List<Frame> frames = this.buildFrames(this.TenPinsRules);
			frames.remove(frames.size() - 1);
			frames.add(FrameDto.builder().attempts(Arrays.asList(10, 10, 10, 10, 10)).prev(Constants.NULL_FRAME).next(Constants.NULL_FRAME).build());
			validator.validateFrame(frames);
		});
		
		assertThrows(Throwable.class, () -> {
			List<Frame> frames = this.buildFrames(this.TenPinsRules);
			frames.remove(frames.size() - 1);
			frames.add(FrameDto.builder().attempts(Arrays.asList(40, 0, 0)).prev(Constants.NULL_FRAME).next(Constants.NULL_FRAME).build());
			validator.validateFrame(frames);
		});

		
		assertThrows(Throwable.class, () -> {
			List<Frame> frames = this.buildFrames(this.TenPinsRules);
			Frame beforeLast = frames.remove(frames.size() - 2);
			Frame spiedFrame = Mockito.spy(beforeLast);
			Mockito.when(spiedFrame.getScore()).thenReturn(50);
			Frame lastFrame = frames.remove(frames.size() - 1);
			frames.add(spiedFrame);
			frames.add(lastFrame);
			validator.validateFrame(frames);
		});
		
		assertThrows(Throwable.class, () -> {
			List<Frame> frames = this.buildFrames(this.TenPinsRules);
			Frame beforeLast = frames.remove(frames.size() - 2);
			Frame spiedFrame = Mockito.spy(beforeLast);
			Mockito.when(spiedFrame.getSummarizedScore()).thenReturn(500);
			Frame lastFrame = frames.remove(frames.size() - 1);
			frames.add(spiedFrame);
			frames.add(lastFrame);
			validator.validateFrame(frames);
		});

	}
}
