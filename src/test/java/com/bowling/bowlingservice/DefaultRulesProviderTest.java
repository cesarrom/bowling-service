package com.bowling.bowlingservice;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

import com.bowling.bowlingservice.business.impl.DefaultRulesProvider;
import com.bowling.bowlingservice.model.Rules;


public class DefaultRulesProviderTest {
	
	@Test
	public void getRules() {
		DefaultRulesProvider provider = new DefaultRulesProvider();
		Rules rules = provider.getRules();
		assertNotEquals(rules, null);
	}
	
}
