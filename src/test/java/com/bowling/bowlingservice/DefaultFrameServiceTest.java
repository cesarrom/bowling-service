package com.bowling.bowlingservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import com.bowling.bowlingservice.business.impl.DefaultFrameFactory;
import com.bowling.bowlingservice.business.impl.DefaultFrameService;
import com.bowling.bowlingservice.business.impl.DefaultGameValidator;
import com.bowling.bowlingservice.model.Frame;
import com.bowling.bowlingservice.model.Rules;
import com.bowling.bowlingservice.model.impl.dto.RulesDto;

public class DefaultFrameServiceTest {

	private final Rules TenPinsRules = RulesDto.builder().frameCount(10).maxAttempts(2).maxExtraAttempts(1)
			.maxScore(300).maxFrameScore(30).pinCount(10).build();

	@Test
	void parseFrames() {
		DefaultGameValidator validator = new DefaultGameValidator(() -> this.TenPinsRules);

		DefaultFrameFactory factory = new DefaultFrameFactory(() -> this.TenPinsRules);

		DefaultFrameService service = new DefaultFrameService(() -> this.TenPinsRules, factory, validator);

		List<Integer> allShots = Arrays.asList(10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10);

		List<Frame> frames = service.parseFrames(allShots);

		assertNotNull(frames);
		assertEquals(frames.size(), 10);

		List<Integer> attempts = frames.stream().flatMap(f -> f.getAttempts().stream()).collect(Collectors.toList());

		assertNotNull(attempts);
		assertEquals(attempts.size(), 12);

		
		allShots = Arrays.asList(9, 0, 8, 2, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10);
		frames = service.parseFrames(allShots);

		assertNotNull(frames);
		assertEquals(frames.size(), 10);

		attempts = frames.stream().flatMap(f -> f.getAttempts().stream()).collect(Collectors.toList());

		assertNotNull(attempts);
		assertEquals(attempts.size(), 14);

	}
}
