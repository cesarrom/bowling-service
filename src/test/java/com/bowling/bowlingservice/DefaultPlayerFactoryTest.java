package com.bowling.bowlingservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.bowling.bowlingservice.business.impl.DefaultPlayerFactory;
import com.bowling.bowlingservice.model.Frame;
import com.bowling.bowlingservice.model.Player;

public class DefaultPlayerFactoryTest {
	@Test
	public void create() {
		DefaultPlayerFactory factory = new DefaultPlayerFactory();
		List<Frame> frames = new ArrayList<>();
		Player player = factory.create("Jhon", frames);
		assertNotEquals(player, null);
		assertEquals(player.getName(), "Jhon");
		assertEquals(player.getFrames(), frames);
	}
}
