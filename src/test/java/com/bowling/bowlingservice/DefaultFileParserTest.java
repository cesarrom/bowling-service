package com.bowling.bowlingservice;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.junit.jupiter.api.Test;

import com.bowling.bowlingservice.business.RulesProvider;
import com.bowling.bowlingservice.business.impl.DefaultFileParser;
import com.bowling.bowlingservice.business.impl.DefaultFrameFactory;
import com.bowling.bowlingservice.business.impl.DefaultFrameService;
import com.bowling.bowlingservice.business.impl.DefaultGameValidator;
import com.bowling.bowlingservice.business.impl.DefaultPlayerFactory;
import com.bowling.bowlingservice.model.Rules;
import com.bowling.bowlingservice.model.impl.dto.RulesDto;

public class DefaultFileParserTest {
	private final Rules TenPinsRules = RulesDto.builder().frameCount(10).maxAttempts(2).maxExtraAttempts(1)
			.maxScore(300).maxFrameScore(30).pinCount(10).build();
	
	BufferedReader stringAsBufferedReader(String str) {
		InputStream targetStream = new ByteArrayInputStream(str.getBytes());	    
	    return new BufferedReader(new InputStreamReader(targetStream));
	}
	
	@Test
	void parse() {  
	    DefaultPlayerFactory playerFactory = new DefaultPlayerFactory();
	    RulesProvider provider = () -> this.TenPinsRules;
	    DefaultFrameFactory factory = new DefaultFrameFactory(provider);
	    DefaultGameValidator validator = new DefaultGameValidator(provider);
	    DefaultFrameService service = new DefaultFrameService(provider, factory, validator);
	    DefaultFileParser parser = new DefaultFileParser(playerFactory, provider, service);
	    
	    assertThrows(Throwable.class, () -> {
	    	BufferedReader bufferedReader = this.stringAsBufferedReader("Wrong format string");
	    	parser.parse(bufferedReader);
	    });
	    
	    assertDoesNotThrow(() -> {
	    	BufferedReader bufferedReader = this.stringAsBufferedReader("\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
	    	parser.parse(bufferedReader);
	    });
	    
	    assertDoesNotThrow(() -> {
	    	BufferedReader bufferedReader = this.stringAsBufferedReader(
	    			""
	    			+ "Jeff 10\r\n"
	    			+ "John 3\r\n"
	    			+ "John 7\r\n"
	    			+ "Jeff 7\r\n"
	    			+ "Jeff 3\r\n"
	    			+ "John 6\r\n"
	    			+ "John 3\r\n"
	    			+ "Jeff 9\r\n"
	    			+ "Jeff 0\r\n"
	    			+ "John 10\r\n"
	    			+ "Jeff 10\r\n"
	    			+ "John 8\r\n"
	    			+ "John 1\r\n"
	    			+ "Jeff 0\r\n"
	    			+ "Jeff 8\r\n"
	    			+ "John 10\r\n"
	    			+ "Jeff 8\r\n"
	    			+ "Jeff 2\r\n"
	    			+ "John 10\r\n"
	    			+ "Jeff F\r\n"
	    			+ "Jeff 6\r\n"
	    			+ "John 9\r\n"
	    			+ "John 0\r\n"
	    			+ "Jeff 10\r\n"
	    			+ "John 7\r\n"
	    			+ "John 3\r\n"
	    			+ "Jeff 10\r\n"
	    			+ "John 4\r\n"
	    			+ "John 4\r\n"
	    			+ "Jeff 10\r\n"
	    			+ "Jeff 8\r\n"
	    			+ "Jeff 1\r\n"
	    			+ "John 10\r\n"
	    			+ "John 9\r\n"
	    			+ "John 0"
	    			);
	    	parser.parse(bufferedReader);
	    });
	    
	}
}
