package com.bowling.bowlingservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

import com.bowling.bowlingservice.business.impl.DefaultFrameFactory;
import com.bowling.bowlingservice.model.Frame;
import com.bowling.bowlingservice.model.Rules;
import com.bowling.bowlingservice.model.impl.dto.RulesDto;

public class DefaultFrameFactoryTest {
	private final Rules TenPinsRules = RulesDto.builder().frameCount(10).maxAttempts(2).maxExtraAttempts(1)
			.maxScore(300).maxFrameScore(30).pinCount(10).build();

	@Test
	void create() {
		DefaultFrameFactory factory = new DefaultFrameFactory(() -> this.TenPinsRules);
		Frame strikeFrame = factory.create(Arrays.asList(10, 0), null, null, 0);
		
		assertNotNull(strikeFrame);
		assertNotNull(strikeFrame.getAttempts());
		assertEquals(strikeFrame.getAttemptSize(), 2);
		assertEquals(strikeFrame.getFrameScore(), 10);
		
		Frame spareFrame = factory.create(Arrays.asList(9, 1), null, null, 0);
		
		assertNotNull(spareFrame);
		assertNotNull(spareFrame.getAttempts());
		assertEquals(spareFrame.getAttemptSize(), 2);
		assertEquals(spareFrame.getFrameScore(), 10);
		
		Frame usualFrame = factory.create(Arrays.asList(0, 1), null, null, 0);
		
		assertNotNull(usualFrame);
		assertNotNull(usualFrame.getAttempts());
		assertEquals(usualFrame.getAttemptSize(), 2);
		assertEquals(usualFrame.getFrameScore(), 1);
		
		Frame lastFrame = factory.create(Arrays.asList(10, 10, 10), null, null, 9);
		
		assertNotNull(lastFrame);
		assertNotNull(lastFrame.getAttempts());
		assertEquals(lastFrame.getAttemptSize(), 3);
		assertEquals(lastFrame.getFrameScore(), 30);
	}
}
