package com.bowling.bowlingservice;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Set;

import org.junit.jupiter.api.Test;

import com.bowling.bowlingservice.business.GameSummaryFactory;
import com.bowling.bowlingservice.business.RulesProvider;
import com.bowling.bowlingservice.business.impl.DefaultFileParser;
import com.bowling.bowlingservice.business.impl.DefaultFrameFactory;
import com.bowling.bowlingservice.business.impl.DefaultFrameService;
import com.bowling.bowlingservice.business.impl.DefaultGameSummaryFactory;
import com.bowling.bowlingservice.business.impl.DefaultGameValidator;
import com.bowling.bowlingservice.business.impl.DefaultPlayerFactory;
import com.bowling.bowlingservice.controller.SummarizeController;
import com.bowling.bowlingservice.controller.impl.DefaultSummarizeController;
import com.bowling.bowlingservice.model.GameSummary;
import com.bowling.bowlingservice.model.Rules;
import com.bowling.bowlingservice.model.impl.dto.RulesDto;

public class DefaultSummarizeControllerTest {

	private final Rules TenPinsRules = RulesDto.builder().frameCount(10).maxAttempts(2).maxExtraAttempts(1)
			.maxScore(300).maxFrameScore(30).pinCount(10).build();

	InputStream stringAsInputStream(String str) {
		return new ByteArrayInputStream(str.getBytes());
	}

	@Test
	public void summarizeFile() {
		DefaultPlayerFactory playerFactory = new DefaultPlayerFactory();
		RulesProvider provider = () -> this.TenPinsRules;
		DefaultFrameFactory factory = new DefaultFrameFactory(provider);
		DefaultGameValidator validator = new DefaultGameValidator(provider);
		DefaultFrameService service = new DefaultFrameService(provider, factory, validator);
		DefaultFileParser parser = new DefaultFileParser(playerFactory, provider, service);
		GameSummaryFactory gameSummaryFactory = new DefaultGameSummaryFactory(provider);

		SummarizeController controller = new DefaultSummarizeController(parser, gameSummaryFactory);

		assertThrows(Throwable.class, () -> {
			controller.summarizeFile(this.stringAsInputStream("Wrong format string"));
		});

		assertDoesNotThrow(() -> {
			Set<GameSummary> result = controller
					.summarizeFile(this.stringAsInputStream("\r\n\r\n\r\n\r\n\r\n\r\n\r\n"));
			assertNotEquals(result, null);
			assertEquals(result.size(), 0);
		});

		assertDoesNotThrow(() -> {
			Set<GameSummary> result = controller.summarizeFile(this
					.stringAsInputStream("" + "Jeff 10\r\n" + "John 3\r\n" + "John 7\r\n" + "Jeff 7\r\n" + "Jeff 3\r\n"
							+ "John 6\r\n" + "John 3\r\n" + "Jeff 9\r\n" + "Jeff 0\r\n" + "John 10\r\n" + "Jeff 10\r\n"
							+ "John 8\r\n" + "John 1\r\n" + "Jeff 0\r\n" + "Jeff 8\r\n" + "John 10\r\n" + "Jeff 8\r\n"
							+ "Jeff 2\r\n" + "John 10\r\n" + "Jeff F\r\n" + "Jeff 6\r\n" + "John 9\r\n" + "John 0\r\n"
							+ "Jeff 10\r\n" + "John 7\r\n" + "John 3\r\n" + "Jeff 10\r\n" + "John 4\r\n" + "John 4\r\n"
							+ "Jeff 10\r\n" + "Jeff 8\r\n" + "Jeff 1\r\n" + "John 10\r\n" + "John 9\r\n" + "John 0"));
			assertNotEquals(result, null);
			assertEquals(result.size(), 2);
			result.forEach(e -> {
				assertNotEquals(e, null);
				assertEquals(e.getExtraAttempts(), this.TenPinsRules.getMaxExtraAttempts());
				assertEquals(e.getSummarizedScore().size(), this.TenPinsRules.getFrameCount());
			});
		});
	}
}
