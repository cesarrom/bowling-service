package com.bowling.bowlingservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import com.bowling.bowlingservice.business.GameSummaryFactory;
import com.bowling.bowlingservice.business.RulesProvider;
import com.bowling.bowlingservice.business.impl.DefaultFileParser;
import com.bowling.bowlingservice.business.impl.DefaultFrameFactory;
import com.bowling.bowlingservice.business.impl.DefaultFrameService;
import com.bowling.bowlingservice.business.impl.DefaultGameSummaryFactory;
import com.bowling.bowlingservice.business.impl.DefaultGameValidator;
import com.bowling.bowlingservice.business.impl.DefaultPlayerFactory;
import com.bowling.bowlingservice.controller.SummarizeController;
import com.bowling.bowlingservice.controller.impl.DefaultSummarizeController;
import com.bowling.bowlingservice.controller.shell.SummarizeShellController;
import com.bowling.bowlingservice.model.Rules;
import com.bowling.bowlingservice.model.impl.dto.RulesDto;

public class SummarizeShellControllerTest {

	private final Rules TenPinsRules = RulesDto.builder().frameCount(10).maxAttempts(2).maxExtraAttempts(1)
			.maxScore(300).maxFrameScore(30).pinCount(10).build();

	private String clean(String str) {
		return str.replaceAll("\\r", "").replaceAll("\\n", "").replaceAll("\\t", "").trim();
	}

	@Test
	public void run() {
		DefaultPlayerFactory playerFactory = new DefaultPlayerFactory();
		RulesProvider provider = () -> this.TenPinsRules;
		DefaultFrameFactory factory = new DefaultFrameFactory(provider);
		DefaultGameValidator validator = new DefaultGameValidator(provider);
		DefaultFrameService service = new DefaultFrameService(provider, factory, validator);
		DefaultFileParser parser = new DefaultFileParser(playerFactory, provider, service);
		GameSummaryFactory gameSummaryFactory = new DefaultGameSummaryFactory(provider);
		SummarizeController controller = new DefaultSummarizeController(parser, gameSummaryFactory);

		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		final String utf8 = StandardCharsets.UTF_8.name();

		try (PrintStream ps = new PrintStream(baos, true, utf8)) {
			final SummarizeShellController shell = new SummarizeShellController(controller, Optional.of(ps));

			ps.flush();
			shell.path = "bowling_game.txt";
			shell.run();
			String out = this.clean(baos.toString(utf8));
			assertTrue(out.indexOf(this.clean("Frames\t\t1\t\t2\t\t3\t\t4\t\t5\t\t6\t\t7\t\t8\t\t9\t\t10\r\n")) >= 0);
			assertTrue(out.indexOf(this.clean("John\r\n")) >= 0);
			assertTrue(out
					.indexOf(this.clean("Pinfalls\t3\t/\t6\t3\t\tX\t8\t1\tX\tX\t9\t0\t7\t/\t4\t4\t10\t9\t0\r\n")) >= 0);
			assertTrue(out.indexOf(
					this.clean("Score\t\t16\t\t25\t\t44\t\t53\t\t82\t\t101\t\t110\t\t124\t\t132\t\t151\r\n")) >= 0);
			assertTrue(out.indexOf(this.clean("Jeff\r\n")) >= 0);
			assertTrue(out.indexOf(
					this.clean("Pinfalls\t\tX\t7\t/\t9\t0\t\tX\t0\t8\t8\t/\t0\t6\t\tX\t\tX\t10\t8\t1\r\n")) >= 0);
			assertTrue(out.indexOf(
					this.clean("Score\t\t20\t\t39\t\t48\t\t66\t\t74\t\t84\t\t90\t\t120\t\t148\t\t167\r\n")) >= 0);
			
			ps.flush();
			shell.path = "../../../../bowling_game.txt";
			shell.run();
			out = this.clean(baos.toString(utf8));
			assertTrue(out.indexOf("File does not exists") >= 0);
			
			ps.flush();
			shell.path = "src";
			shell.run();
			out = this.clean(baos.toString(utf8));
			assertTrue(out.indexOf("A File was expected but a directoy was provided") >= 0);
			
			ps.flush();
			shell.path = null;
			shell.run();
			out = this.clean(baos.toString(utf8));
			assertTrue(out.indexOf("No path was provided, remember using '-p' param to provide a path string to the file to summarize") >= 0);

		} catch (Throwable ex) {
			assertEquals(ex, null);
		}

	}
}
