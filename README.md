### Command Line tool for bowling games
#### Developers:
* compilation is achieved by running `build.sh` file
* the output jar with all its dependencies is located at `compiled/bowlingService.jar`
* this projects uses mainly `lombok`, `spring-boot` and `picocli`
* this project follows `SOA` or `Layered` structure
* dependencies are managed by `maven`
* project includes tests which triggers automatically when you run `build.sh`

#### Users:
* when you have access to the `bowlingService.jar`, you can use the service by running `java -jar bowlingService.jar summarize` which receives a required parameter `-p` which is a string that represents the absolute or relative path to the file that you want to summarize

* the file must be a simple text file

* an example usage could be `java -jar bowlingService.jar summarize -p="C:\\Users\\my_user\\Desktop\\bowling_game.txt"`

